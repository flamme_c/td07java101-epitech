package net.epitech.java.td03.dao;

import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import net.epitech.java.td03.model.SmartCard;

/**
 * provide the ability to supply a datasource to the dao
 * 
 * @author nicolas
 * 
 */
public abstract class AbstractSmartCardDAO implements SmartCardDAO {

	protected DataSource datasource;

	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}

	public AbstractSmartCardDAO(DataSource datasource) {
		this.datasource = datasource;
	}

	public AbstractSmartCardDAO() {

	}

	// debug dump
	public static void showList(Collection<SmartCard> cards) {

		for (SmartCard aCard : cards) {
			System.out.println(aCard.getId() + " " + aCard.getType() + " "
					+ aCard.getHolderName());

		}

	}

}
