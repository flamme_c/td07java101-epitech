package net.epitech.java.td.service.impl;

import java.util.Collection;

import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;

import net.epitech.java.td.domain.Access;
import net.epitech.java.td.domain.Course;
import net.epitech.java.td.domain.Teacher;
import net.epitech.java.td.exception.FailedToParseDayInWeekException;
import net.epitech.java.td.exception.FailedToParseMailException;
import net.epitech.java.td.exception.NoSuchCourseException;
import net.epitech.java.td.exception.NoSuchTeacherException;
import net.epitech.java.td.repositories.CourseRepository;
import net.epitech.java.td.repositories.TeacherRepositoy;
import net.epitech.java.td.repositories.custom.TeacherRepositoryCustom;
import net.epitech.java.td.service.EpitechService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EpitechServiceImpl implements EpitechService {

	@Inject
	CourseRepository courseRepo;

	@Inject
	TeacherRepositoy teacherRepo;

	@Inject
	TeacherRepositoryCustom custom;

	@Inject
	EntityManagerFactory emFactory;

	@Inject
	PasswordEncoder passwordEncoder;

	public Collection<Course> getCourses() {
		return courseRepo.findAll();

	}

	public Collection<Teacher> getTeachers() {
		return teacherRepo.findAll();

	}

	public void deleteCourse(Integer id) throws NoSuchCourseException {
		Course course = courseRepo.findOne(id);
		if (course == null)
			throw new NoSuchCourseException("course already deleted");
		courseRepo.delete(course);

	}

	public Teacher addTeacher(String name, String mail, String password)
			throws FailedToParseMailException {

		Teacher teacher = new Teacher();
		teacher.setName(name);
		teacher.setMail(mail);
		teacher.setPwd(passwordEncoder.encode(password));

		teacher = teacherRepo.save(teacher);
		teacherRepo.flush();
		LOGGER.info("Un nouveau prof a été créé: %", name);
		return teacher;
	}

	public void deleteTeacher(Integer id) throws Exception {
		// TODO Auto-generated method stub

	}

	private static final Logger LOGGER = LoggerFactory
			.getLogger(EpitechServiceImpl.class);

	@Override
	public Collection<Teacher> getTeacherWithNoCourses() {
		return custom.getUnemployedTeachers();

	}

	@Override
	public void addTeacherWithCourse(String name, String email,
			String password, Integer cours) throws FailedToParseMailException {
		Teacher teacher = this.addTeacher(name, email, password);
		if (cours != null) {
			Course course = courseRepo.findOne(cours);
			course.setTeacher(teacher);
			courseRepo.saveAndFlush(course);
		}

	}

	@Override
	public Course getCourseById(Integer courseId) throws NoSuchCourseException {
		Course course = courseRepo.findOne(courseId);
		if (course == null)
			throw new NoSuchCourseException("cannot find course");
		return course;
	}

	@Override
	public void detachCourseFromTeacher(Integer courseId, Integer teacherId)
			throws NoSuchTeacherException, NoSuchCourseException {
		final Teacher t = teacherRepo.findOne(teacherId);
		final Course course = courseRepo.findOne(courseId);

		if (t == null)
			throw new NoSuchTeacherException("teacher doesn't exist");
		if (course == null)
			throw new NoSuchCourseException("course doesn't exist");

		if (course.getTeacher().getId() == t.getId()) {
			course.setTeacher(null);
			courseRepo.saveAndFlush(course);
		} else {
			throw new NoSuchTeacherException(
					"teacher doesn't teach this course");
		}

	}

	@Transactional
	@Override
	public Teacher getTeacher(Integer idTeacher) {
		Teacher t = teacherRepo.findOne(idTeacher);

		return t;
	}

	@Transactional
	@Override
	public Course addCourse(String name, String dayInWeek, Integer duration,
			String teacherMail) throws FailedToParseDayInWeekException {

		LOGGER.trace("Je suis rentré  dans la fonction addCourse");
		Course course = new Course();
		if (dayInWeek.toLowerCase().equals("lundi")) {
			course.setDayInWeek(1);
		} else {
			LOGGER.warn("Mauvais jour de la semaine %s", dayInWeek);
			throw new FailedToParseDayInWeekException();
		}

		Teacher t = teacherRepo.findByMail(teacherMail);

		if (t != null) {
			for (Access access : t.getAccesses()) {
				if (access.getName().equals("TEACHER")) {
					course.setTeacher(t);
					break;
				}
			}
		} else {
			LOGGER.warn("cours crée sans prof");
		}
		course.setDuration(duration);

		course.setName(name);

		course = courseRepo.saveAndFlush(course);
		LOGGER.info("Un nouveau cours a été crée %s", course.getName());
		return course;

	}

}
