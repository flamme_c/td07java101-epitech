package net.epitech.java.td.config.security;

import java.util.Arrays;
import java.util.Collection;

import javax.inject.Inject;

import net.epitech.java.td.domain.Access;
import net.epitech.java.td.domain.Teacher;
import net.epitech.java.td.repositories.TeacherRepositoy;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

//this class use data access to provide a way to authenticate users
@Service
public class EpitechUserDetailService implements UserDetailsService {

	@Inject
	TeacherRepositoy teacherRepo;

	@Inject
	PasswordEncoder passwordEncoder;

	@Transactional
	// @Transactional annotation is used to access lazy initialization for
	// domain object
	@Override
	public UserDetails loadUserByUsername(String mail)
			throws UsernameNotFoundException {

		// master authentication (hard coded for convenience)
		if ("test@test.com".equals(mail)) {
			SimpleGrantedAuthority authority = new SimpleGrantedAuthority(
					"ADMIN");
			User user = new User("test@test.com",
					passwordEncoder.encode("test"), Arrays.asList(authority));
			return user;

		}

		// find a teacher by mail
		Teacher teachers = teacherRepo.findByMail(mail);
		if (teachers == null) {
			// bad credentials
			throw new UsernameNotFoundException(mail);
		}

		// from the Access table, we get roles as strings and convert them as
		// Granted Authority, using Guava
		Collection<GrantedAuthority> accessAsGrantedAuthority = Lists
				.newArrayList(Iterables.transform(teachers.getAccesses(),
						new Function<Access, GrantedAuthority>() {

							@Override
							public GrantedAuthority apply(Access access) {
								SimpleGrantedAuthority authority = new SimpleGrantedAuthority(
										access.getName());
								return authority;

							}

						}));

		// plug user info and well as authorities
		User user = new User(teachers.getMail(), teachers.getPwd(),
				accessAsGrantedAuthority);
		return user;
	}
}
