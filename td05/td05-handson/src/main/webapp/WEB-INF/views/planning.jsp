<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="resources/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/bootstrap-theme.min.css" rel="stylesheet"
	type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Planning des cours</title>
</head>
<body>

	<c:forEach var="cours" items="${courses}">
		<ol class="breadcrumb">	
				<li><c:out value="${cours.name}"></c:out></li>
				<li><fmt:formatDate value="${cours.date.toDate()}" pattern="EEEE" /></li>
				<li><c:out value="${cours.duration.getStandardHours()} h"></c:out></li>
		</ol>
	</c:forEach>
	
</body>
</html>