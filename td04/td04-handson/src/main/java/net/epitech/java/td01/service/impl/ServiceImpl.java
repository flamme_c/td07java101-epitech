package net.epitech.java.td01.service.impl;

import java.util.Collection;
import java.util.HashSet;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import net.epitech.java.td01.model.Course;
import net.epitech.java.td01.model.Teacher;
import net.epitech.java.td01.service.contract.Service;

//TODO:this class should be unit tested
@org.springframework.stereotype.Service
public class ServiceImpl implements Service {
	public ServiceImpl() {
		this.idCourse = 0;
		this.idTeacher = 0;
		this.courses = new HashSet<Course>();
		this.teachers = new HashSet<Teacher>();

		try {
			Teacher chuck = this.addTeacher("Norris", "norris_c@epitech.eu");
			Teacher noel = this.addTeacher("Noel", "noel_p@epitech.eu");

			this.addCourse("KungFu", new DateTime(), new Duration(
					3600 * 3 * 1000), chuck);
			this.addCourse("Boxe", new DateTime(),
					new Duration(3600 * 3 * 1000), chuck);

			this.addCourse("Dinde au marron", new DateTime(), new Duration(
					3600 * 3 * 1000), noel);
		} catch (Exception e) {
			// for tests.. should not throw
			new RuntimeException(e);
		}

	}

	public Collection<Course> getCourses() {
		return this.courses;
	}

	public Collection<Teacher> getTeachers() {
		return this.teachers;
	}

	public Course addCourse(String name, DateTime date, Duration duration,
			Teacher t) throws Exception {

		for (Course c : this.courses) {
			if (false) // TODO: chevauchement
				throw new Exception("Course already exist");
		}

		Course c = new Course();
		c.setId(this.idCourse++);
		c.setName(name);
		c.setDate(date);
		c.setDuration(duration);
		c.setTeacher(t);
		this.courses.add(c);
		return c;

	}

	public void deleteCourse(Integer id) throws Exception {
		boolean exist = false;
		Collection<Course> coursetodelete = new HashSet<Course>();

		for (Course c : this.courses) {
			if (c.getId().equals(id)) {
				coursetodelete.add(c);
				exist = true;
			}
		}
		this.courses.removeAll(coursetodelete);
		if (!exist)
			throw new Exception("Course does not exist");
	}

	public Teacher addTeacher(String name, String mail) throws Exception {
		boolean exist = false;

		for (Teacher t : this.teachers) {
			if ((t.getName() + t.getMail()).equals((name + mail)))
				exist = true;
		}
		if (!exist) {
			Teacher t = new Teacher();
			t.setId(this.idTeacher++);
			t.setName(name);
			t.setMail(mail);
			this.teachers.add(t);
			return t;
		} else
			throw new Exception("Teacher already exist");
	}

	public void deleteTeacher(Integer id) throws Exception {
		boolean exist = false;
		Collection<Teacher> teacherstodelete = new HashSet<Teacher>();

		for (Teacher t : this.teachers) {
			if (t.getId().equals(id)) {
				teacherstodelete.add(t);
				exist = true;
			}
		}
		this.teachers.removeAll(teacherstodelete);
		if (!exist)
			throw new Exception("Teacher does not exist");
	}

	public Collection<Pair<DateTime, DateTime>> getAvaibleTimeSlot(Duration d) {
		Collection<Pair<DateTime, DateTime>> avaibleTimeSlot = new HashSet<Pair<DateTime, DateTime>>();
		return avaibleTimeSlot;
	}

	private Collection<Course> courses;
	private Collection<Teacher> teachers;
	private Integer idTeacher;
	private Integer idCourse;

}
